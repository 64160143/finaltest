/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.purin.finalproject;

/**
 *
 * @author tin
 */
public class Bmisaver {
    private double height;
    private double weitght;
    private  double bmi;

    public Bmisaver(double height, double weitght, double bmi) {
        this.height = height;
        this.weitght = weitght;
        this.bmi = bmi;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeitght() {
        return weitght;
    }

    public void setWeitght(double weitght) {
        this.weitght = weitght;
    }

    public double getBmi() {
        return bmi;
    }

    public void setBmi(double bmi) {
        this.bmi = bmi;
    }

    @Override
    public String toString() {
        return "Bmisaver{" + "height=" + height + ", weitght=" + weitght + ", bmi=" + bmi + '}';
    }
    
}
